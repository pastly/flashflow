.. FlashFlow documentation master file, created by
   sphinx-quickstart on Tue Jun 16 11:21:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FlashFlow: A Secure Speed Test for Tor
======================================

FlashFlow is a new distributed bandwidth measurement system for Tor that
consists of a single authority node ("coordinator") instructing one or more
measurement nodes ("measurers") when and how to measure Tor relays.  It's
results can be used directly as relay weights, or they can be used as much more
accurate relay capacity estimates in another system such as TorFlow or Simple
Bandwidth Scanner.

System Overview
---------------

The following diagram shows a FlashFlow deployment in action as it measurers
two relays.  Note how multiple measurers can work together to measure a relay
(e.g. the top relay) if, for example, they do not individually possess enough
bandwidth capacity to do it alone. Also note how one measurer can measure
multiple relays at the same time (e.g. the bottom measurer).

.. image:: imgs/flashflow.svg

The coordinator instructs its Tor client to connect to the relays and tell them
the measurement parameters (blue connections). The relays agree to be measured, and
the coordinator tells the appropriate measurers (over the green connections) to
connect to the relays with many TLS connections. The measurers instruct
their Tor clients to do so (thus establishing red connections), and when done, inform
the coordinator of their success over the green connections. Once all red connections are
established for the measurement of a relay, the coordinator tells the measurers
to begin measuring, they forward this command to their Tor clients, and
measurement traffic starts getting sent to the relay.

Relays echo measurement traffic back to measurers on the red connections, and
for every second during the measurement, report to the coordinator on the blue
connections how much background client traffic they forwarded in the preceding
second. When a relay sends its last background traffic report, it closes all
red connections and the blue connection.

The coordinator logs all per-second measurement data from measurers and relays
into an intermediate results file. Periodically it generates a v3bw file using
the latest measurement results for each relay. More specifically, the
coordinator sums the per-second reported throughputs into 30 sums (assuming a
30 second measurement) and uses the median of these sums as the weight for each
relay listed in the v3bw file.


.. toctree::
   :maxdepth: 2
   :caption: More information on these processes can be found here:

   SPECS

.. toctree::


Code
----

- https://gitlab.torproject.org/pastly/flashflow
- https://gitlab.torproject.org/pastly/stem (branch: *flashflow*)
- https://gitlab.torproject.org/pastly/flashflow-tor

Docs
----

- **This page**: `http://jsd33qlp6p2t[...].onion
  <http://jsd33qlp6p2t3snyw4prmwdh2sukssefbpjy6katca5imn4zz4pepdid.onion>`_ AKA
  https://flashflow.pastly.xyz.
- **Unit test coverage**: `http://jsd33qlp6p2t[...].onion/coverage
  <http://jsd33qlp6p2t3snyw4prmwdh2sukssefbpjy6katca5imn4zz4pepdid.onion/coverage>`_
  AKA https://flashflow.pastly.xyz/coverage.

Literature
----------

- `FlashFlow: A Secure Speed Test for Tor <https://arxiv.org/pdf/2004.09583.pdf>`_
- `Tor Proposal #316 <https://gitweb.torproject.org/torspec.git/tree/proposals/316-flashflow.txt>`_

--------------------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   DEPLOY
   HACKING
   SPECS
   License (CC0) <LICENSE>

.. toctree::
   :maxdepth: 3
   :caption: Code:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
