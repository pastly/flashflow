import unittest
from flashflow.relay_filter_list import RFLWord, RelayFilterList as RFL


class TestRFLWord(unittest.TestCase):
    def test_empty(self):
        for word in ['', '        ', '\t\t']:
            line = RFLWord(word)
            assert not line.is_negative
            assert not line.is_wildcard

    def test_comment_is_bad(self):
        with self.assertRaises(AssertionError):
            RFLWord('#foo')
            RFLWord('# foo')

    def test_negative(self):
        word = RFLWord('!fingerprint')
        assert word.is_negative
        assert word.s == 'fingerprint'

        word = RFLWord('fingerprint')
        assert not word.is_negative
        assert word.s == 'fingerprint'

    def test_wilcard(self):
        assert RFLWord('*').is_wildcard
        assert RFLWord('!*').is_wildcard

    def test_normalize(self):
        fp = 'fingerprint'
        assert RFLWord(fp).s == fp
        assert RFLWord(fp.upper()).s == fp
        assert RFLWord(' ' + fp + '\t\t').s == fp
        assert RFLWord(' ' + fp.upper() + '\t\t').s == fp


class TestRFL:
    def test_empty(self):
        for s in ['', '\n\n\n', '   \n # foo  ']:
            rfl = RFL.from_str(s)
            assert not len(rfl.words)

    def test_comment_line(self):
        s = '# foo\n\n # foo \n bar\n#foo'
        rfl = RFL.from_str(s)
        assert len(rfl.words) == 1
        assert rfl.words[0].s == 'bar'

    def test_end_of_line_comment(self):
        s = '   bar    #foo\n'
        rfl = RFL.from_str(s)
        assert len(rfl.words) == 1
        assert rfl.words[0].s == 'bar'

    def test_multi_word_line(self):
        s = 'foo     bar baz'
        rfl = RFL.from_str(s)
        assert len(rfl.words) == 3
        assert [w.s for w in rfl.words] == ['foo', 'bar', 'baz']

    def test_multi_lines(self):
        s = 'foo  \n   bar\nbaz'
        rfl = RFL.from_str(s)
        assert len(rfl.words) == 3
        assert [w.s for w in rfl.words] == ['foo', 'bar', 'baz']

    def test_should_measure_wildcard(self):
        ''' Wildcard always matches even if another valid match comes later '''
        for wild, fp, should, default in [
            ('*', '!foo', True, True),
            ('!*', 'foo', False, True),
            ('*', '!foo', True, False),
            ('!*', 'foo', False, False),
        ]:
            rfl = RFL.from_str(f'{wild}\n{fp}')
            assert rfl.should_measure('foo', default) == should

    def test_should_not_measure_match(self):
        ''' Exact match always matches even if a wilcard comes later '''
        for wild, fp, should, default in [
            ('*', '!foo', False, True),
            ('!*', 'foo', True, True),
            ('*', '!foo', False, False),
            ('!*', 'foo', True, False),
        ]:
            rfl = RFL.from_str(f'{fp} {wild}')
            assert rfl.should_measure('foo', default) == should

    def test_default(self):
        ''' Default is used when no match '''
        rfl = RFL.from_str('foo')
        assert rfl.should_measure('bar', default=True)
        assert not rfl.should_measure('bar', default=False)
