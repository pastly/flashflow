import flashflow.meas_period as mp


MEAS_TO_SLOT_FACT = 2
VERY_SMALL = 0.000000001


class TestPeriod:
    def test_current_period(self):
        ''' Verify times that fall in the 0'ith measurement period '''
        for dur in [1, 30, 500, 86400, 17]:
            assert mp.current_period(0, dur) == 0
            assert mp.current_period(VERY_SMALL, dur) == 0
            assert mp.current_period(dur-VERY_SMALL, dur) == 0
            assert mp.current_period(dur/2, dur) == 0
            # this is the moment it ticks over to the next period
            assert mp.current_period(dur, dur) == 1

    # ## The function tested is commented out as not needed in real code
    # def test_time_till_next_period(self):
    #     VERY_SMALL = 0.000000001
    #     DUR = 30
    #     assert mp.time_till_next_period(0, DUR) == DUR
    #     assert mp.time_till_next_period(VERY_SMALL, DUR) == DUR - VERY_SMALL
    #     # Mathematically, should be equal to VERY_SMALL, but floats are
    #     # imperfect, so just make sure it's positive and pretty close to
    #     # VERY_SMALL
    #     assert mp.time_till_next_period(DUR-VERY_SMALL, DUR) > 0 and \
    #         mp.time_till_next_period(DUR-VERY_SMALL, DUR) < VERY_SMALL * 1.1
    #     assert mp.time_till_next_period(DUR, DUR) == DUR


class TestSlot:
    def test_current_slot_edges(self):
        P_DUR = 86400
        M_DUR = 30
        n_slots = int(P_DUR / M_DUR / MEAS_TO_SLOT_FACT)
        # Very beginning of period
        assert mp.current_slot(0, P_DUR, M_DUR) == 0
        # Very beginning plus a tiny bit
        assert mp.current_slot(0+VERY_SMALL, P_DUR, M_DUR) == 0
        # Very end minus a little bit
        assert mp.current_slot(P_DUR-VERY_SMALL, P_DUR, M_DUR) == n_slots - 1
        # Very end is actually the beginning of the next
        assert mp.current_slot(P_DUR, P_DUR, M_DUR) == 0

    def test_current_slot_each(self):
        P_DUR = 86400
        M_DUR = 30
        n_slots = int(P_DUR / M_DUR / MEAS_TO_SLOT_FACT)
        for slot_n in range(n_slots):
            # Very beginning of slot
            assert mp.current_slot(
                M_DUR * MEAS_TO_SLOT_FACT * slot_n, P_DUR, M_DUR) == slot_n
            # Very beginning of slot, plus a little bit
            assert mp.current_slot(
                M_DUR * MEAS_TO_SLOT_FACT * slot_n + VERY_SMALL,
                P_DUR, M_DUR) == slot_n
            # Very end of slot minus a little bit
            assert mp.current_slot(
                M_DUR * MEAS_TO_SLOT_FACT * (slot_n+1) - VERY_SMALL,
                P_DUR, M_DUR) == slot_n
            # Very end of slot is actually next (or 0 if rollover)
            assert mp.current_slot(
                M_DUR * MEAS_TO_SLOT_FACT * (slot_n+1),
                P_DUR, M_DUR) == (slot_n + 1) % n_slots

    def test_time_till_next_slot(self):
        VERY_SMALL = 0.000000001
        M_DUR = 30
        slot_dur = M_DUR * MEAS_TO_SLOT_FACT
        assert mp.time_till_next_slot(0, M_DUR) == slot_dur
        assert mp.time_till_next_slot(VERY_SMALL, M_DUR) == \
            slot_dur - VERY_SMALL
        # Mathematically, should be equal to VERY_SMALL, but floats are
        # imperfect, so just make sure it's positive and pretty close to
        # VERY_SMALL
        assert mp.time_till_next_slot(slot_dur-VERY_SMALL, M_DUR) > 0 and \
            mp.time_till_next_slot(slot_dur-VERY_SMALL, M_DUR) < \
            VERY_SMALL * 1.1
        assert mp.time_till_next_slot(slot_dur, M_DUR) == slot_dur
