import asyncio
import enum
import logging
import unittest
from unittest.mock import MagicMock
from flashflow.cmd import coord


loop = asyncio.get_event_loop()
CIRC_ID = 69
MEAS_ID = 420
RELAY_FP = 'RELAYFP'


def toy_measurement():
    return coord.Measurement(MEAS_ID, RELAY_FP, set(), 10, 0.1)


class MockTorController(MagicMock):
    pass


def e(s: str):
    ''' Parse Tor control event string *s* into its stem :class:`Event`
    subclass '''
    from stem.response import ControlMessage
    return ControlMessage.from_str(s, msg_type='EVENT', normalize=True)


class EvGenType(enum.Enum):
    ''' The types of events :class:`EvGen` can generate

    The dict argument that EvGen expects for each EvGenType has different
    contents depending on which EvGenType is given:

    - SLEEP: dur: a float
    - FUNC: f: the function to call, a: list of positional arguments, kw: dict
    of keyword arguments. *a* and *kw* are optional
    '''
    SLEEP = enum.auto()
    FUNC = enum.auto()


class EvGen:
    ''' Class that generates events/messages

    :param events: list of tuples. The first item of each tuple is a
        :class:`EvGenType`, and the second item is an dict, whose contents
        depend on the type of event.
    '''
    def __init__(self, events):
        self._events = events

    async def go(self):
        for ev_type, ev_args in self._events:
            logging.debug('Generating %s with args %s', ev_type, ev_args)
            if ev_type == EvGenType.SLEEP:
                await asyncio.sleep(ev_args['dur'])
            elif ev_type == EvGenType.FUNC:
                a = ev_args['a'] if 'a' in ev_args else []
                kw = ev_args['kw'] if 'kw' in ev_args else {}
                ev_args['f'](*a, **kw)


def rand_listen_addr():
    from random import randint
    # return '[::1]:' + str(randint(10000, 64000))
    return 'localhost:' + str(randint(10000, 64000))


# def loop():
#     import asyncio
#     return asyncio.get_event_loop()


class Base(unittest.TestCase):
    ''' Abstract out the some state creation for tests in this file '''
    def setUp(self):
        from flashflow.cmd.coord import State
        from flashflow.config import get_config
        from tempfile import TemporaryDirectory
        self.datadir = TemporaryDirectory()
        conf = get_config(None)
        conf['coord']['datadir'] = self.datadir.name
        conf['coord']['keydir'] = 'tests/data/coord/keys'
        conf['coord']['listen_addr'] = rand_listen_addr()
        self.state = State(conf)
        self.setUp_logging()

    def setUp_logging(self):
        logging.getLogger().level = logging.DEBUG

    def tearDown(self):
        self.datadir.cleanup()
        self.tearDown_logging()

    def tearDown_logging(self):
        pass


class TestConnectToRelay(Base):
    ''' Test the various paths we can take through
    :meth:`coord.connect_to_relay` '''
    def setUp(self):
        super().setUp()
        self.meas = toy_measurement()

    def test_already_have_circ(self):
        ''' We return early when the :class:`coord.Measurement` already has a
        circuit recorded '''
        # sanity check
        assert self.meas.relay_circ is None
        # sanity check: start without any measurements
        self.assertEqual(len(self.state.measurements), 0)
        # pretend there already is a relay circ
        self.meas.relay_circ = 1
        # make sure error is logged
        with self.assertLogs(level='ERROR') as log_ctx:
            coord.connect_to_relay(self.meas, self.state)
        self.assertIn('already has circ', '\n'.join(log_ctx.output))
        # and that we still don't have the measurement saved in state
        self.assertEqual(len(self.state.measurements), 0)

    def test_cannot_launch_circ(self):
        '''  We return early when our Tor client cannot even begin to connect
        to the relay '''
        # sanity check: start without any measurements
        self.assertEqual(len(self.state.measurements), 0)
        # Fake the response as being not 250
        c = MockTorController()
        c.msg.return_value = e('552 Not the real error message')
        self.state.attach_sockets(c, None, None)
        # make sure error is logged
        with self.assertLogs(level='ERROR') as log_ctx:
            coord.connect_to_relay(self.meas, self.state)
        self.assertIn('Failed to launch', '\n'.join(log_ctx.output))
        # and that we still don't have the measurement saved in state
        self.assertEqual(len(self.state.measurements), 0)

    def test_weird_ok_response_too_many_words(self):
        ''' The response is 250 (ok), but still not as expected '''
        # sanity check: start without any measurements
        self.assertEqual(len(self.state.measurements), 0)
        # supposed to be '250 LAUNCHED 1'
        c = MockTorController()
        c.msg.return_value = e('250 LAUNCHED 1 2')
        self.state.attach_sockets(c, None, None)
        # make sure error is logged
        with self.assertLogs(level='ERROR') as log_ctx:
            coord.connect_to_relay(self.meas, self.state)
        self.assertIn('Did not expect body', '\n'.join(log_ctx.output))
        # and that we still don't have the measurement saved in state
        self.assertEqual(len(self.state.measurements), 0)

    def test_weird_ok_response_not_launched(self):
        ''' The response is 250 (ok), but still not as expected '''
        # sanity check: start without any measurements
        self.assertEqual(len(self.state.measurements), 0)
        # supposed to be '250 LAUNCHED 1'
        c = MockTorController()
        c.msg.return_value = e('250 FOOOED 1')
        self.state.attach_sockets(c, None, None)
        # make sure error is logged
        with self.assertLogs(level='ERROR') as log_ctx:
            coord.connect_to_relay(self.meas, self.state)
        self.assertIn('Did not expect body', '\n'.join(log_ctx.output))
        # and that we still don't have the measurement saved in state
        self.assertEqual(len(self.state.measurements), 0)

    def test_success(self):
        c = MockTorController()
        c.msg.return_value = e('250 LAUNCHED ' + str(CIRC_ID))
        self.state.attach_sockets(c, None, None)
        # make sure log message exists
        with self.assertLogs(level='INFO') as log_ctx:
            coord.connect_to_relay(self.meas, self.state)
        s = 'Meas %d launched circ %d' % (
            self.meas.meas_id, self.meas.relay_circ)
        self.assertIn(s, '\n'.join(log_ctx.output))
        # we now have the measurement saved
        self.assertEqual(len(self.state.measurements), 1)


class TestFullMeas(Base):
    ''' "Perform" a full measurement, feeding the coord all the events it
    expects along the way. '''
    def setUp(self):
        super().setUp()
        self.meas = toy_measurement()

    def test_full(self):
        s = self.state
        c = MockTorController()
        c.msg.return_value = e('250 LAUNCHED ' + str(CIRC_ID))
        self.state.attach_sockets(c, None, None)
        coord.connect_to_relay(self.meas, self.state)
        self.assertEqual(len(self.state.measurements), 1)
        ev_gen = EvGen([
            (EvGenType.FUNC, {'f': coord.notif_circ_event, 'a': [e('650 CIRC %d LAUNCHED BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686' % (CIRC_ID,)), s]}),
            (EvGenType.FUNC, {'f': coord.notif_circ_event, 'a': [e('650 CIRC %d EXTENDED $3C1C2A0700314E39E903C16D87727D7D1ED0C7C2~relay1 BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686' % (CIRC_ID,)), s]}),
            (EvGenType.FUNC, {'f': coord.notif_circ_event, 'a': [e('650 CIRC %d BUILT $3C1C2A0700314E39E903C16D87727D7D1ED0C7C2~relay1 BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686' % (CIRC_ID,)), s]}),
            (EvGenType.FUNC, {'f': coord.notif_ffmeas_event, 'a': [e('650 FF_MEAS %d PARAMS_OK ACCEPTED=YES' % (MEAS_ID,)), s]}),
        ])
        loop.run_until_complete(ev_gen.go())
        assert False




# class TestFoo(Base):
#     ''' Test reaction to FF_MEAS Tor control events '''
#     def test_foo(self):
#         s = self.state
#         s.measurements = {1: coord.Measurement(
#             1, 'RELAYFP', set(), 10, 0.1)}
#         ev_gen = EvGen([
#             (EvGenType.FUNC, {'f': coord.notif_circ_event, 'a': [e('650 CIRC 7 LAUNCHED BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686'), s]}),
#             (EvGenType.FUNC, {'f': coord.notif_circ_event, 'a': [e('650 CIRC 7 EXTENDED $3C1C2A0700314E39E903C16D87727D7D1ED0C7C2~relay1 BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686'), s]}),
#             (EvGenType.FUNC, {'f': coord.notif_circ_event, 'a': [e('650 CIRC 7 BUILT $3C1C2A0700314E39E903C16D87727D7D1ED0C7C2~relay1 BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686'), self.state]}),
#         ])
#         loop.run_until_complete(ev_gen.go())
#         assert False

# CIRC 7 LAUNCHED BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686
# Meas 2860533786 sent params on circ 7 to relay 3C1C2A0700314E39E903C16D87727D7D1ED0C7C2
# CIRC 7 EXTENDED $3C1C2A0700314E39E903C16D87727D7D1ED0C7C2~relay1 BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686
# CIRC 7 BUILT $3C1C2A0700314E39E903C16D87727D7D1ED0C7C2~relay1 BUILD_FLAGS=ONEHOP_TUNNEL PURPOSE=GENERAL TIME_CREATED=2020-10-08T02:00:16.098686
# Found meas 2860533786 waiting on circ 7 to be built. Not doing anything yet.
