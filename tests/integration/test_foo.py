import os
import unittest
from tempfile import TemporaryDirectory
from flashflow import tor_client

NET_TAR = 'tests/integration/net.txz'


def _torrc_for_testnet_client(torrc_common: str):
    ''' Given the filename for the testnet's torrc-common file containing
    the ``DirAuthority`` lines, return the extra torrc lines necessary to
    connect to this testnet. I.e. those ``DirAuthority`` lines and
    ``TestingTorNetwork 1``.
    '''
    out = 'TestingTorNetwork 1\n'
    with open(torrc_common, 'rt') as fd:
        for line in fd:
            if line.startswith('DirAuthority'):
                out += line
    return out


class Base(unittest.TestCase):
    ''' Abstract out the some state creation for tests in this file '''
    TOR_BIN = 'tor'

    def setUp(self):
        self.tempdir = TemporaryDirectory()
        self.setUp_network()

    def setUp_network(self):
        import subprocess
        import time
        # Extract network
        cmd = 'tar xf {fname} --directory={tmpdir}'.format(
            fname=NET_TAR,
            tmpdir=self.tempdir.name)
        assert subprocess.run(cmd.split()).returncode == 0
        # Start network
        net_dname = os.path.join(self.tempdir.name, 'net')
        cmd = './02-start-network.sh'
        ret = subprocess.run(
            cmd, cwd=net_dname,
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        assert ret.returncode == 0
        # Check it has bootstrapped
        time.sleep(1)
        cmd = './03-network-in-ready-state.py -d auth* guard* middle* exit*'
        ret = subprocess.run(
            # need shell=True so the globbing in the command works
            cmd, cwd=net_dname, shell=True,
            stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        assert ret.returncode == 0

    def tearDown(self):
        import subprocess
        import time
        net_dname = os.path.join(self.tempdir.name, 'net')
        subprocess.run(['./04-stop-network.sh'], cwd=net_dname)
        time.sleep(0.100)
        self.tempdir.cleanup()


class BaseWClient(Base):
    ''' Base, but also launch a Tor client '''
    def setUp(self):
        super().setUp()
        self.datadir = TemporaryDirectory()
        torrc_common = os.path.join(self.tempdir.name, 'net', 'torrc-common')
        torrc_extra = _torrc_for_testnet_client(torrc_common)
        self.tor_client = tor_client.launch(
            self.TOR_BIN, self.datadir.name, torrc_extra)

    def tearDown(self):
        super().tearDown()
        self.datadir.cleanup()


class TestLaunchTor(Base):
    ''' Can launch a tor client and successfully bootstrap '''

    def test_basic_testnet_tor(self):
        ''' Can launch a tor client that connects to our local test network '''
        torrc_common = os.path.join(self.tempdir.name, 'net', 'torrc-common')
        torrc_extra = _torrc_for_testnet_client(torrc_common)
        with TemporaryDirectory() as datadir:
            c = tor_client.launch(self.TOR_BIN, datadir, torrc_extra)
            assert c.is_authenticated()
            line = c.get_info('status/bootstrap-phase')
            state, _, progress, *_ = line.split()
            assert state == 'NOTICE'
            progress = int(progress.split('=')[1])
            assert progress == 100


class TestNetworkInfo(BaseWClient):
    ''' Can successfully query our client for information regarding relays,
    etc. '''
    BAD_RELAY_NICK = 'NickNotReal'
    GOOD_RELAY_NICK = 'guard1'
    GOOD_RELAY_FP = '4600CFEF48FC3923FA9FA6AFE2AEEE0F97B22B8A'

    def test_find_relay(self):
        from flashflow.cmd.coord import find_relay
        # can't find bad nick
        relay = find_relay(self.tor_client, self.BAD_RELAY_NICK)
        assert relay is None
        # can find good nick
        relay = find_relay(self.tor_client, self.GOOD_RELAY_NICK)
        assert relay.nickname == self.GOOD_RELAY_NICK
        assert relay.fingerprint == self.GOOD_RELAY_FP
        # can find good fp
        relay = find_relay(self.tor_client, self.GOOD_RELAY_FP)
        assert relay.nickname == self.GOOD_RELAY_NICK
        assert relay.fingerprint == self.GOOD_RELAY_FP
