''' State file '''
import cbor2  # type: ignore
from typing import Dict, Any, Optional
import gzip
import logging
import os


log = logging.getLogger(__name__)
VERSION: int = 1


class StateFile:
    #: The data
    d: Dict[str, Any]
    #: The filename we were loaded from, if any
    fname: Optional[str]

    def __init__(self):
        self.d = {
            'version': VERSION,
        }
        self.fname = None

    @staticmethod
    def from_file(fname: str) -> 'StateFile':
        ''' Load a state object from the given filename. If the file doesn't
        exist, just return a new object. '''
        state = StateFile()
        state.fname = fname
        if not os.path.exists(fname):
            return state
        with gzip.open(fname, 'rb') as fd:
            state.d = cbor2.load(fd)
        if state.d['version'] > VERSION:
            log.warn(
                'Loaded state from %s with version %d, but the latest '
                'version we know is %d. Bad things may happen.',
                fname, state.d['version'], VERSION)
        if state.d['version'] < VERSION:
            log.warn(
                'Need to update state format in %s from %d to %d, but nothing '
                'to do that has been written yet.',
                fname, state.d['version'], VERSION)
        state.fname = fname
        return state

    def to_file(self, fname: Optional[str] = None):
        ''' Write ourselves out to the given filename, overwriting anything
        that might already exist there.

        - If no file is given and we don't know what file we were read from, do
          nothing.
        - If no file is given but we do know from where we were read, write out
          to that file.
        - If a file is given, write out to that regardless of where we were
          read (if anywhere).
        '''
        if not fname and not self.fname:
            return
        if not fname:
            # log.debug('Writing state to %s', self.fname)
            with gzip.open(self.fname, 'wb') as fd:
                cbor2.dump(self.d, fd)
            return
        assert fname
        # log.debug('Writing state to %s', fname)
        with gzip.open(fname, 'wb') as fd:
            cbor2.dump(self.d, fd)

    def set(self, key: str, val: Any, skip_write: bool = False):
        ''' Set ``key`` to ``val``, and write out this change to the state
        file, unless ``skip_write`` is set to ``True``.
        '''
        # log.debug('Setting %s => %s', key, val)
        self.d[key] = val
        if not skip_write:
            self.to_file()

    def get(self, key: str, default: Any = None) -> Any:
        ''' Get the value stored at ``key``, or the provided ``default`` value
        if there is no such key. By default, ``default`` is ``None``. '''
        if key not in self.d:
            return default
        return self.d[key]
