'''
Measurement Periods and Slots
==============================

Helper functions for calculating things about measurement periods and slots.

Measurement **Period**: Suggested to be a day, each relay is intended to be
measured once each period.  At the beginning of each period (or on startup, if
started mid-period without an existing schedule for that period), the
coordinator comes up with  a schedule that it will follow for the rest of the
period, measuring relays in slots.

Measurement **Slots**: Twice the length of the active measurement duration,
slots subdivide a measurement period. The first slot of each period is 0, and
there are ``N`` slots in a period, where ``N = meas_period / (meas_dur * 2)``.
Relays get scheduled into measurement slots, and the extra length allows for
wiggle room for overhead due to measurement creation (building 100+ circuits
takes a while!) and any other causes.

Here is a diagram of what happens during Period ``M``, which is divided into
``N`` slots from ``0`` to ``N-1``. Slot 1 is further blown up to show how three
relays are scheduled to be measured during it. The pre-measurement work for
each took a different amount of time, which is okay: that's why slots are twice
as long as the actual measurement duration. The moment the work is done to
measure a relay, the measurement is started. There is still time left in the
slot after the relays are measured. This is okay; we just wait until the next
slot starts before doing any measurements scheduled in it.

::

    ------------ Time ------------>

    |--Period M-------------------------------------------------|--Period M+1--
    |--Slot 0----|--Slot 1----| ... |--Slot N-2----|--Slot N-1--|--Slot 0----
                /             \\
              /                 \\
            /                     -----------------------------------------\\
          /                                                                 |
          |--Pre-meas relay1--|--Meas relay1--------------|                 |
          |--Pre-meas relay2----|--Meas relay2--------------|               |
          |--Pre-meas relay3----------|--Meas relay3--------------|         |

It's possible a slot doesn't have any measurements in it. That's fine. We just
wait until the next slot.
'''
import logging
import random
from typing import List, Dict, Tuple


log = logging.getLogger(__name__)
#: How much larger is a slot than a single measurement's duration? E.g. ``2``
#: here means 30 second measurements are scheduled into 60 second slots.
MEAS_TO_SLOT_FACT = 2


def _time_till_next(now: float, dur: int) -> float:
    ''' Calculate time till the next ``dur`` period of time starts

    :param now: The current timestamp
    :parm dur: The slice size used to divide time

    :returns: The time remaining in the current slice of time
    '''
    current_slice = int(now / dur)
    next_slice = current_slice + 1
    return next_slice * dur - now


def current_period(now: float, period_dur: int) -> int:
    ''' Calculate the measurement period number and return it.

    :param now: The current timestamp
    :param period_dur: The duration of a measurement period

    :returns: The measurement period number in which ``now`` resides.
    '''
    return int(now / period_dur)


# ## Unit test is written for this, but commented out
# def time_till_next_period(now: float, period_dur: int) -> float:
#     ''' Return the time remaining until the next measurement period starts.
#
#     :param now: The current timestamp
#     :param period_dur: The duration of a measurement period
#     '''
#     return _time_till_next(now, period_dur)


def current_slot(now: float, period_dur: int, meas_dur: int) -> int:
    ''' Calculate the slot number and return it.

    :param now: The current timestamp
    :param period_dur: The duration of a measurement period
    :param meas_dur: The duration of a measurement

    :returns: The slot number in which ``now`` resides.
    '''
    slot_dur = meas_dur * MEAS_TO_SLOT_FACT
    time_since_period_begin = now - period_dur*current_period(now, period_dur)
    return int(time_since_period_begin / slot_dur)


def time_till_next_slot(now: float, meas_dur: int) -> float:
    ''' Calculate the time remaining until the next measurement slot starts.

    :param now: The current timestamp
    :param meas_dur: The duration of a measurement period
    '''
    return _time_till_next(now, meas_dur * MEAS_TO_SLOT_FACT)


class MeasrInfo:
    ''' Store general information on a measurer in one object to easily pass it
    around.

    :param measr_id: The measurer's ID
    :param bw: The amount of bandwidth, in **bytes**/second, the measurer is
        capable of
    '''
    def __init__(self, measr_id: str, bw: int):
        self.measr_id = measr_id
        self.bw = bw


class RelayInfo:
    ''' Store general information on a relay in one object to easily pass it
    around.

    :param fp: The relay's fingerprint
    '''
    def __init__(self, fp: str):
        self.fp = fp


class MeasrMeasInfo:
    ''' Store info associated to how a specific measurer participates in a
    specific measurement.

    :param measr_id: A unique ID for the measurer that will be still be
        meaningful hours after making this object, ideally across reconnection
        with the measurer. In practice, we require measurers to use a unique
        ``organizationName`` in their certificate and use that.
    :param n_circs: The number of circuits this measurer shall open with the
        relay.
    :param bw: The amount of bandwidth, in bytes/second, the measurer should
        allocate to measuring this relay.
    '''
    def __init__(self, measr_id: str, n_circs: int, bw: int):
        self.measr_id = measr_id
        self.n_circs = n_circs
        self.bw = bw

    def to_dict(self) -> Dict:
        return {
            'measr_id': self.measr_id,
            'n_circs': self.n_circs,
            'bw': self.bw,
        }

    @staticmethod
    def from_dict(d: Dict) -> 'MeasrMeasInfo':
        return MeasrMeasInfo(
            d['measr_id'],
            d['n_circs'],
            d['bw'],
        )


class Schedule:
    ''' Measurement Schedule for a Measurement Period.

    :param relays: List of relays to schedule during the
        measurement period
    :param measurers: List of :class:`MeasrInfo` we
        should use this measurement period
    :param n_slots: The number of slots there are in a measurement period
    :param n_circs: The number of circuits the measurers, in aggregate, should
        open with a relay to measure it
    '''
    #: Key is slot number, value is a list tuples containing information needed
    #: for each measurement.
    #:
    #: The contents of the tuple:
    #:  1. :class:`str`, the fingerprint of the relay to measure
    #:  2. List of :class:`MeasrMeasInfo` for the measurers to use for this
    #:     measurement
    #:
    #: Not every slot number will be in this dict. Missing slots have no
    #: measurements scheduled.
    slots: Dict[int, List[Tuple[str, List[MeasrMeasInfo]]]]

    def __init__(self):
        ''' Generate an empty schedule. You probably want :meth:`Schedule.gen`
        '''
        self.slots = {}

    @staticmethod
    def gen(
            relays: List[RelayInfo], measurers: List[MeasrInfo],
            n_slots: int, n_circs: int) -> 'Schedule':
        sched = Schedule()
        log.info(
            'Building new %d-slot measurement schedule of %d relays with '
            '%d measurers capable of a combined %d Mbit/s',
            n_slots, len(relays), len(measurers),
            sum([m.bw for m in measurers]) * 8 / 1e6,  # bytes to bits, then M
        )
        # Shuffle the relays and take at most n_slots of them
        if len(relays) <= n_slots:
            log.debug(
                'Putting a maximum of one relay in each slot.')
            random.shuffle(relays)
        else:
            log.warn(
                'Not enough slots available to simply measure one relay in '
                'each. Will measure %d relays (selected uniformly at random) '
                'and forego the rest', n_slots)
            relays = random.sample(relays, n_slots)
        # choose a random set of slots to use
        slot_ids = random.sample(range(n_slots), len(relays))
        for slot_id, relay in zip(slot_ids, relays):
            measurer = random.choice(measurers)
            sched.slots[slot_id] = [(
                relay.fp,
                [MeasrMeasInfo(measurer.measr_id, n_circs, measurer.bw)],
            )]
            log.debug(
                'Will measure %s with %s in slot %d',
                relay.fp, measurer.measr_id, slot_id)
        log.info(
            'Finished generating schedule. Scheduled %d measurements in '
            '%d slots', sum([len(_) for _ in sched.slots.values()]),
            len(sched.slots))
        return sched

    def to_dict(self) -> Dict[int, List[Tuple[str, List[Dict]]]]:
        out: Dict[int, List[Tuple[str, List[Dict]]]] = {}
        for key, measurements in self.slots.items():
            out[key] = []
            for relay_fp, measurers in measurements:
                out[key].append((
                    relay_fp,
                    [m.to_dict() for m in measurers],
                ))
        return out

    @staticmethod
    def from_dict(d: Dict[int, List[Tuple[str, List[Dict]]]]) -> 'Schedule':
        sched = Schedule()
        for key, measurements in d.items():
            sched.slots[key] = []
            for relay_fp, measurers in measurements:
                sched.slots[key].append((
                    relay_fp, [MeasrMeasInfo.from_dict(m) for m in measurers]))
        return sched
