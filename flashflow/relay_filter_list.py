'''
Relay Filter List
=================

Parse a relay filter list file and decide whether or not we should measure a
relay based on its fingerprint.

Files are line and word based and read left-to-right, top-to-bottom. First word
to match a given fingerprint wins.

Syntax
------

Everything after a ``#`` is a comment:

.. code-block:: text

    # This is a comment
    this is not a comment # but this is
    other stuff

A word starting with ``!`` is a negative match, meaning that if the rest of
the word matches, then the relay should **not** be measured. Normally a match
means *yes, do measure*.

.. code-block:: text

    !DoNotMeasureThisFP
    DoMeasureThisFP

A word containing a ``*`` is a wildcard word, meaning it matches all
fingerprints.

.. code-block:: text

    *   # means measure all relays
    !*  # means do not measure any relay

Relay fingerprints are the only other valid non-comment text that should be in
this type of file.

.. warning::
    You may find that non-fingerprint text is parsed as fingerprints if they
    are not in a comment. Be careful. Don't do this.

You can have multiple words/fingerprints per line. These snippets are parsed
the exactly same way. They demonstrate a config where 3 relays have opted-in to
being measured and no other relay should be measured.

.. code-block:: text

    # First
    RelayFP1
    RelayFP2
    RelayFP3
    !*

    # Second
    RelayFP1 RelayFP2 RelayFP3
    !*

    # Third
    RelayFP1 RelayFP2 RelayFP3 !*

    RelayFP1 RelayFP2 RelayFP3 !*   # Forth


Examples
--------

.. note::
    For brevity, pretend that relay fingerprints are four alphanumeric
    characters in the following examples.

Do not measure any relay, ever::

    !*

Measure all relays except one::

    !FFFF
    *

Only measure one relay::

    AAAA !*

Maybe two people have opted in to be measured and you want to organize their
relays by their families::

    # Jacob's relays
    AAAA BBBB CCCC
    DDDD EEEE FFFF

    # Paul's relays
    PPP1 PPP2 PPP3
    # Paul said we shouldn't measure this one
    !PPP4

    !*

'''
from typing import List


def normalize_fp(s: str) -> str:
    ''' Normalize a fingerprint so no matter the format in which it is
    received, it will be in a consistent format for later comparisons to
    work.

    Currently this function is used for things that may not be exactly a
    fingerprint: comment lines, and wildcard words are two examples. Don't edit
    this function without verifying this is no longer the case or that what you
    want to do won't break those other things.
    '''
    return s.strip().lower()


class RFLWord:
    ''' A single word read from a file. You should not not need to use this
    directly.  '''
    #: Whether this is a negative-match word or not. If ``True``, then if the
    #: fingerprint matches, the relay should **NOT** be measured.
    is_negative: bool = False
    #: Whether this is a wildcard word or not. If ``True``, then this word
    #: matches all fingerprints.
    is_wildcard: bool = False

    def __init__(self, s: str):
        # Perhaps a redundant action, but better safe than sorry.
        s = normalize_fp(s)
        assert not s.startswith('#')
        # Determine if this is a negative-match word, and remove the '!' if
        # needed
        self.is_negative = False
        if s.startswith('!'):
            self.is_negative = True
            s = s[1:]
        # Determine if this is a wildcard word, and remove the '*' if needed
        self.is_wildcard = False
        if s.startswith('*'):
            self.is_wildcard = True
            s = s[1:]
        # Save what remains of the word
        self.s = s

    def matches(self, s: str) -> bool:
        ''' Determine if the given string matches this :class:`RFLWord` '''
        if self.is_wildcard:
            return True
        return normalize_fp(s) == self.s


class RelayFilterList:
    #: Ordered list of word we read from the file
    words: List[RFLWord]

    def __init__(self):
        self.words = []

    @staticmethod
    def from_str(s: str) -> 'RelayFilterList':
        ''' Given the entire string contents of a file, return a new
        :class:`RelayFilterList`
        '''
        filter_list = RelayFilterList()
        for line in s.split('\n'):
            line = normalize_fp(line)
            # Remove both entire comment lines and end-of-line comments.
            if '#' in line:
                line = line[:line.find('#')]
            assert '#' not in line
            # Non-comment line. If empty, nothing to do
            if not len(line):
                continue
            # To support multiple fingerprints per line, act on each word
            # individually
            for word in line.split():
                filter_list.words.append(RFLWord(normalize_fp(word)))
        return filter_list

    def should_measure(self, fp: str, default: bool) -> bool:
        ''' Determine whether or not the given ``fp`` should be measured. If no
        match is found, then return ``default`` '''
        for word in self.words:
            if word.matches(fp):
                return not word.is_negative
        return default
