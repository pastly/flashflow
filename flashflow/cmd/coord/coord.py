'''
FlashFlow coordinator
=====================

The FlashFlow coordinator is the brain of a FlashFlow team. It controls one or
more measurers, telling them when and how to perform bandwidth measurements of
relays.

Code organization
-----------------

The coordinator first opens its listening sockets for measurers and for the
FlashFlow controller. Then it launches a Tor client and connects and
authenticates to it. These things are done in :meth:`main`.

At this point, events drive the execution of the program. The following are the
message/event types:

- begin of slot/begin of period events,
- FF ctrl messages,
- CIRC Tor control messages,
- FF_MEAS Tor control messages, and
- FF measurer messages.

The remainder of this section explains the organization of the code and control
flow by walking the reader through the process of a measurement taking place.

Begin of slot/period events
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Main function(s):

- :meth:`begin_of_slot_cb`
- :meth:`begin_of_new_period`
- :meth:`schedule_next_begin_of_slot`

The measurement schedule divides time into periods (i.e. days) and periods into
slots (i.e. minutes). We schedule a call to :meth:`begin_of_slot_cb` at the
beginning of every slot with :meth:`schedule_next_begin_of_slot`. If it's also
the beginning of a new period, :meth:`begin_of_new_period` is also called.

If there are measurements to do, :meth:`begin_of_slot_cb` sends a FF ctrl
message for each to start them.

FlashFlow control messages
^^^^^^^^^^^^^^^^^^^^^^^^^^

Main function(s):

- :meth:`notif_ctrl_message` and the similarly named helper functions it calls

:meth:`notif_ctrl_message` incoming FlashFlow control messages. These are
simple strings and either come from a FlashFlow control process (not to be
confused with a Tor controller or Tor control port) or internally. The most
notable message is ``measure <fingerprint>``, which instructs us to start
measuring the relay with the given fingerprint. A :class:`Measuremet` is
constructed and :meth:`connect_to_relay` is called to start the measurement by
telling our tor client to construct a one-hop circuit to the relay for
measurement purposes, at which point we stop doing things related to this
measurement until our Tor client reports its progress via CIRC and FF_MEAS Tor
control messages.

CIRC Tor control messages
^^^^^^^^^^^^^^^^^^^^^^^^^

Main function(s):

- :meth:`notif_circ_event` and the similarly named helper functions it calls

These come from stem in :meth:`_notif_circ_event`, but in a different thread.
Thus we have to send the event to the main event loop thread in
:meth:`notif_circ_event`.

From there, control goes to a different function based on the status of the
circuit (e.g. BUILT or LAUNCHED or CLOSED). But ultimately, the coordinator
doesn't really care: its Tor client knows to send measurement parameters to the
relay after the circuit is BUILT, so all we do is debug-log when the circuit is
BUILT.

The message we're ultimately waiting for is a FF_MEAS event.

FF_MEAS Tor control messages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Main function(s):

- :meth:`notif_ffmeas_event` and the similarly named helper functions it calls

These come from stem in :meth:`_notif_ffmeas_event`, but in a different thread.
Thus we have to send the event to the main event loop thread in
:meth:`notif_ffmeas_event`.

In the running example of a measurement taking place, we're waiting for a
PARAMS_OK FF_MEAS message. Upon receiving it we hand control off to a
sub-function that determines if the relay agrees to be measured, and if so,
sends messages to the appropriate measurers instructing them to connect to the
relay.

FlashFlow measurer messages
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Main function(s):

- :meth:`notif_measurer_msg`
- :meth:`notif_measurer_connected`
- :meth:`notif_measurer_disconnected`

We're waiting for measurers to respond saying they've successfully connected to
the relay. Once :meth:`_notif_measurer_msg_ConnectedToRelay` has seen the
:class:`ConnectedToRelay` from each measurer, it tells our tor client that
active measurement is starting and tells each measurer to start active
measurement.

Per-second messages
-------------------

During a measurement, we expect bandwidth reports each second from our tor
client and from each measurer. The former is a FF_MEAS Tor control message with
type BW_REPORT handled in :meth:`notif_ffmeas_event_BW_REPORT`, and the latter
is a :class:`flashflow.msg.BwReport` FlashFlow measurer message.

'''
from argparse import ArgumentParser
import asyncio
import glob
import logging
import os
import random
import ssl
import time
from asyncio.base_events import Server
from configparser import ConfigParser
from functools import partial
from traceback import StackSummary
from statistics import median
from tempfile import NamedTemporaryFile
from typing import Tuple, List, IO, Set, Dict, Optional, Union
from ... import meas_period
from ... import msg
from ... import results_logger
from ... import tor_client
from ... import v3bw
from ... import __version__ as FF_VERSION, PKG_DIR
from ...relay_filter_list import RelayFilterList
from ...state_file import StateFile
from ...tor_ctrl_msg import CoordStartMeas
from stem import CircStatus  # type: ignore
from stem.control import Controller, EventType  # type: ignore
import stem.descriptor.router_status_entry as stem_rse  # type: ignore
from stem.response.events import CircuitEvent, FFMeasEvent  # type: ignore

RELAYS_TXT_FNAME = os.path.join(PKG_DIR, 'relays.txt')


def next_meas_id() -> int:
    ''' Generate a new measurement ID '''
    return random.randint(1, 2**32-1)


def find_relay(c: Controller, nick_fp: str) \
        -> Optional[stem_rse.RouterStatusEntryV3]:
    ''' Ask our tor client for the :class:`RouterStatusEntryV3` object for the
    given relay, identified either by nickname or fingerprint. If impossible,
    (e.g. it doesn't exist), return ``None``.
    '''
    return c.get_network_status(nick_fp, default=None)


def get_measr_infos(conf: ConfigParser) -> List[meas_period.MeasrInfo]:
    ''' Parse the information we have on our measurers from our config
    file, and return a list of :class:`MeasrInfo` for them.  '''
    out: List[meas_period.MeasrInfo] = []
    measr_ids_str = conf.get('coord', 'measurers').strip()
    if not measr_ids_str:
        return out
    def_bw = conf.getint('measr_default', 'measr_bw')
    for measr_id in measr_ids_str.split(','):
        out.append(meas_period.MeasrInfo(
            measr_id,
            conf.getint(
                'measr_' + measr_id,
                'measr_bw',
                fallback=def_bw),
        ))
    return out


def get_relays_to_measure(c: Controller, rfl_fname: str) \
        -> List[meas_period.RelayInfo]:
    ''' Return the list of relay fingerprints of all known relays that we
    should measure.

    :param c: Tor controller
    :param rfl_fname: Filename containing a :class:`RelayFilterList`
    '''
    relays = []
    with open(rfl_fname, 'rt') as fd:
        rfl = RelayFilterList.from_str(fd.read())
        for ns in c.get_network_statuses():
            fp = ns.fingerprint
            if rfl.should_measure(fp, default=False):
                relays.append(meas_period.RelayInfo(fp))
    return relays


def slots_in_period(conf: ConfigParser) -> int:
    ''' Calculate the number of slots in a measurement period based on the
    given configuration '''
    period = conf.getint('coord', 'meas_period')
    dur = conf.getint('meas_params', 'meas_duration')
    return int(period / (dur * 2))


class MeasrProtocol(asyncio.Protocol):
    ''' How we communicate with measurers.

    Very little should be done here. Parse the bytes then give objects to the
    rest of coord code to handle.
    '''
    transport: Optional[asyncio.Transport] = None

    def connection_made(self, transport):
        global g_state
        # TODO: log host:port of measurer
        self.transport = transport
        # Set measurer ID to the organizationName in the measurer's certificate
        self.measr_id = self.transport.get_extra_info(
            'peercert')['subject'][0][0][1]
        log.info('Connection from measurer "%s"', self.measr_id)
        notif_measurer_connected(self, g_state)

    def connection_lost(self, exc):
        global g_state
        log.debug('Lost connection with measurer')
        notif_measurer_disconnected(self, g_state)
        # TODO: anything else need to be done?

    def data_received(self, data: bytes):
        # XXX: It's possible we receive an incomplete JSON string
        # https://gitlab.torproject.org/pastly/flashflow/-/issues/10
        log.info('Received %d bytes: %s', len(data), data)
        m = msg.FFMsg.deserialize(data)
        notif_measurer_msg(self, m)


class CtrlProtocol(asyncio.Protocol):
    ''' Development/debugging control communication protocol '''
    transport: Optional[asyncio.Transport] = None

    def connection_made(self, transport):
        # TODO: log host:port of controller
        log.debug('Connection from controller')
        self.transport = transport

    def connection_lost(self, exc):
        log.debug('Lost connection with controller')

    def data_received(self, data: bytes):
        # log.debug('ctrl: %s', data)
        assert self.transport is not None
        success, err_str = notif_ctrl_message(data.decode('utf-8'))
        if success:
            self.transport.write(b'OK')
        else:
            self.transport.write(err_str.encode('utf-8'))
        self.transport.close()


class Measurement:
    ''' State related to a single measurment. '''
    #: Measurement ID
    meas_id: int
    #: The fingerprint of the relay to measure
    relay_fp: str
    #: Our circuit id with the relay. Filled in once we actually have one
    relay_circ: Optional[int]
    #: The measurers that have indicated the are ready to begin active
    #: measurement
    ready_measurers: Set[MeasrProtocol]
    #: The duration, in seconds, that active measurement should last.
    meas_duration: int
    #: The percent of background traffic, as a fraction between 0 and 1, that
    #: the relay should be limiting itself to.
    bg_percent: float
    #: Per-second reports from the relay with the number of the bytes of
    #: background traffic it is carrying. Each tuple is ``(time, sent_bytes,
    #: received_bytes)`` where time is the timestamp at which the report was
    #: received and sent/received are from the relay's perspective.
    bg_reports: List[Tuple[float, int, int]]
    #: Per-second reports from the measurers with the number of bytes of
    #: measurement traffic. Each tuple is ``(time, sent_bytes,
    #: received_bytes)``, where time is the timestamp at which the report was
    #: received and sent/received are from the measurer's perspective.
    measr_reports: Dict[MeasrProtocol, List[Tuple[float, int, int]]]

    def __init__(
            self, meas_id: int, relay_fp: str,
            measurers: Set[MeasrProtocol],
            meas_duration: int, bg_percent: float):
        self.meas_id = meas_id
        self.relay_fp = relay_fp
        self.relay_circ = None  # we build the circ and get a circ id later
        self.measurers = measurers
        self.ready_measurers = set()
        self.meas_duration = meas_duration
        self.bg_percent = bg_percent
        self.bg_reports = []
        self.measr_reports = {m: [] for m in self.measurers}

    def start_and_end(self) -> Tuple[float, float]:
        ''' Return our best idea for what the start and end timestamp of this
        :class:`Measurement` are.

        This method assumes the measurement is finished.

        We currently only consider the timestamps in the background reports;
        these have timestamps that *we* generated, so if we always use only
        ourself as the authority on what time it is, we'll always be
        consistent. Consider a deployment with at lesat two measurers, one with
        a very fast clock and another with a very slow clock. If we instead,
        for example, took the earliest timestamp as the start and the latest
        timestamp as the end, then not only could the measurement "last" more
        than its actual duration, but our accuracy on start/end times would
        change as the set of measureers used for each measurement changes.
        '''
        start_ts = self.bg_reports[0][0]
        end_ts = self.bg_reports[-1][0]
        return start_ts, end_ts

    def should_save_data(self) -> bool:
        ''' Return our best idea for whether or not it would be a good idea to
        save this measurement's results to disk. '''
        return not not len(self.bg_reports)


class State:
    ''' Global state for the coord process '''
    #: Config file
    conf: ConfigParser
    #: All measurers that are currently connected
    measurers: Set[MeasrProtocol]
    #: Our Tor client, set in main
    tor_client: Optional[Controller]
    #: Our listening sockets for measurers, set in main
    measr_server: Optional[Server]
    #: Our listening sockets for FF controllers, set in main
    ctrl_server: Optional[Server]
    #: If we have one, the handle for the event to be called at the beginning
    #: of the next measurement slot
    begin_of_slot_handle: Optional[asyncio.TimerHandle]
    #: Storage of ongoing :class:`Measurement`\s
    measurements: Dict[int, Measurement]
    #: On-disk storage of long term information
    state_file: StateFile
    #: Our current measurement schedule
    schedule: Optional[meas_period.Schedule]

    def __init__(self, conf: ConfigParser):
        self.conf = conf
        self.measurers = set()
        self.tor_client = None
        self.measr_server = None
        self.ctrl_server = None
        self.begin_of_slot_handle = None
        self.measurements = {}
        self.state_file = StateFile.from_file(conf.getpath('coord', 'state'))
        self.schedule = meas_period.Schedule.from_dict(
            self.state_file.get('meas_schedule', default={}))

    def attach_sockets(
            self, tor_client: Controller, measr_server: Server,
            ctrl_server: Server):
        ''' Add to the :class:`State` object the actual communication stuff.
        Ideally the object doesn't already have them, but if it does, we'll
        warm '''
        if self.tor_client:
            log.warn(
                'State already has a tor client. Forgetting about it without '
                'any cleanup')
        self.tor_client = tor_client
        if self.measr_server:
            log.warn(
                'State already has a measr server. Forgetting about it '
                'without any cleanup')
        self.measr_server = measr_server
        if self.ctrl_server:
            log.warn(
                'State already has a ctrl server. Forgetting about it '
                'without any cleanup')
        self.ctrl_server = ctrl_server


log = logging.getLogger(__name__)
loop = asyncio.get_event_loop()
#: global in-memory state object, init in main
g_state = None


def _exception_handler(loop, context):
    ''' Last resort exception handler

    This will only catch exceptions that happen in the main thread. Others very
    well may go entirely unnoticed and unlogged.

    Some exceptions are unexpected, so we end up here. For these we kill
    ourselves after logging about the exception.

    Other exceptions are impossible to catch before we get here. For example, a
    client failing the TLS handshake with us. (ugh what the fuck). For these we
    notify the rest of coord code so it can react.
    '''
    # Check for exceptions that should not be fatal and we should tell other
    # parts of the code about so they can react intelligently
    if 'exception' in context:
        exception_type = type(context['exception'])
        # Check for recoverable TLS errors
        if exception_type == ssl.SSLError:
            if 'transport' in context:
                notif_sslerror(context['exception'], context['transport'])
                return
            else:
                log.warn(
                    'SSLError caught without a transport too. Cannot pass ' +
                    'to state machine to handle gracefully.')
        # Additional recoverable errors would continue here
    # All other exceptions. These are fatal
    log.error('%s', context['message'])
    if 'exception' in context:
        log.error('%s %s', type(context['exception']), context['exception'])
    if 'handle' in context:
        log.error(context['handle'])
    if 'source_traceback' in context:
        log.error('Traceback:')
        summary = StackSummary.from_list(context['source_traceback'])
        for line_super in summary.format():
            # The above line has multiple lines in it
            for line in line_super.split('\n'):
                if len(line):
                    log.error('  %s', line)
    else:
        log.error(
            'Traceback not available. Maybe run with PYTHONASYNCIODEBUG=1')
    global g_state
    cleanup(g_state)
    die()


def _gen_concated_measr_cert_file(
        d: str, coord_fname: str) -> Tuple[IO[str], str]:
    ''' Search for measurer certs in the given directory (being careful to
    ignore any file matching the given coord cert filename). Read them all into
    a new temporary file and return its name. Will always return a filename,
    even if it is empty. '''
    cert_fnames = _measr_cert_files(d, coord_fname)
    # + indicates "updating" AKA reading and writing
    fd = NamedTemporaryFile('w+')
    for cert in cert_fnames:
        with open(cert, 'rt') as fd_in:
            fd.write(fd_in.read())
    fd.seek(0, 0)
    log.debug('Stored %d measurer certs in %s', len(cert_fnames), fd.name)
    return fd, fd.name


def _measr_cert_files(d: str, coord_fname: str) -> List[str]:
    ''' Look in the directory `d` for files ending with '.pem', recursively. If
    any found file matches `coord_fname` by name exactly, then ignore it.
    Return all other files found. If no allowed files are found, returns an
    empty list. '''
    out = []
    for fname in glob.iglob(os.path.join(d, '*.pem'), recursive=True):
        if fname == coord_fname:
            continue
        log.debug('Treating %s as a measurer cert file', fname)
        out.append(fname)
    return out


def _ensure_relays_txt(conf):
    fname = conf.getpath('coord', 'relay_filter_list')
    if os.path.exists(fname):
        return
    with open(fname, 'wt') as fd_out, open(RELAYS_TXT_FNAME, 'rt') as fd_in:
        fd_out.write(fd_in.read())


def notif_circ_event_BUILT(meas: Measurement, event: CircuitEvent):
    ''' Received CIRC event with status BUILT.

    Look for a Measurement waiting on this circ_id to be BUILT. '''
    assert event.status == CircStatus.BUILT
    circ_id = int(event.id)
    log.debug(
        'Found meas %d waiting on circ %d to be built. Not doing '
        'anything yet.', meas.meas_id, circ_id)


def notif_circ_event_CLOSED(meas: Measurement, event: CircuitEvent):
    pass


def notif_circ_event_FAILED(meas: Measurement, event: CircuitEvent):
    pass


def notif_circ_event(event: CircuitEvent, state: Optional[State] = None):
    ''' The real CIRC event handler, in the main thread's loop.

    Receive a CIRC event from our tor client.

    We want to know about circuit events for the following reasons:
        - When we have recently launched our circuit with the relay and want to
          know when it is built so we can go to the next state
        - TODO failures
        - TOOD other reasons
    '''
    global g_state
    if not state:
        state = g_state
    # Try to find a Measurement that is interested in this CIRC event based
    # on the circ_id
    matching_meas_ids = [
        meas_id for meas_id, meas in state.measurements.items()
        if meas.relay_circ == int(event.id)]
    # If none, then it's probably our tor client doing irrelevant things.
    # Ignore.
    if not len(matching_meas_ids):
        return
    # If more than one, serious programming issue. Drop.
    if len(matching_meas_ids) != 1:
        log.error(
            'It should not be possible for more than one Measurement to '
            'be using the same circuit id %d. Not handling CIRC event: %s',
            int(event.id), event)
        return
    # Found it. Pass off control based on the type of CIRC event
    meas_id = matching_meas_ids[0]
    meas = state.measurements[meas_id]
    if event.status == CircStatus.BUILT:
        return notif_circ_event_BUILT(meas, event)
    elif event.status in [CircStatus.LAUNCHED, CircStatus.EXTENDED]:
        # ignore these
        return
    elif event.status == CircStatus.CLOSED:
        return notif_circ_event_CLOSED(meas, event)
    elif event.status == CircStatus.FAILED:
        return notif_circ_event_FAILED(meas, event)
    log.warn('Not handling CIRC event: %s', event)


def _notif_circ_event(event: CircuitEvent):
    ''' Called from stem to tell us about CIRC events. We care about these
    when we are waiting on a circuit to be built with a relay.

    We are currently in a different thread. We tell the main thread's loop
    (in a threadsafe manner) to handle this event.
    '''
    loop.call_soon_threadsafe(partial(notif_circ_event, event))


def notif_ffmeas_event_PARAMS_SENT(meas: Measurement, event: FFMeasEvent):
    ''' Received FF_MEAS event with type PARAMS_SENT

    Log about it. There's nothing to do until we learn the relay's response,
    which we get later with PARAMS_OK. '''
    log.info(
        'Meas %d sent params on circ %d to relay %s',
        meas.meas_id, meas.relay_circ, meas.relay_fp)
    return


def notif_ffmeas_event_PARAMS_OK(
        meas: Measurement, event: FFMeasEvent, state: State):
    ''' Received FF_MEAS event with type PARAMS_OK

    Check if the relay accepted them or not. Drop the Measurement if not
    accepted, otherwise continue on to the next stage: telling the measurers to
    connect to the relay.  '''
    if not event.accepted:
        # TODO: record as failed somehow pastly/flashflow#18
        log.warn(
            'Meas %d params not accepted: %s',
            meas.meas_id, event)
        del state.measurements[meas.meas_id]
        return
    assert event.accepted
    log.debug(
        'Meas %d params accepted. Telling measurers to connect', meas.meas_id)
    # TODO: num circs as a param pastly/flashflow#11
    # TODO: bw as a param pastly/flashflow#24
    n_circs = 10
    bw = int((40*10**6)/8)  # 40 Mbit/s as bytes
    m = msg.ConnectToRelay(
        meas.meas_id, meas.relay_fp, n_circs, bw, meas.meas_duration)
    for measr in meas.measurers:
        assert measr.transport is not None
        measr.transport.write(m.serialize())
    return


def notif_ffmeas_event_BW_REPORT(meas: Measurement, event: FFMeasEvent):
    ''' Received FF_MEAS event with type BW_REPORT '''
    meas.bg_reports.append((time.time(), event.sent, event.recv))
    if have_all_bw_reports(meas):
        return finish_measurement(meas, None)
    return


def notif_ffmeas_event(event: CircuitEvent, state: Optional[State] = None):
    ''' The real FF_MEAS event handler, in the main thread's loop.

    Receive a FF_MEAS event from our tor client.
    '''
    global g_state
    if not state:
        state = g_state
    meas_id = event.meas_id
    if meas_id not in state.measurements:
        log.error(
            'Received FF_MEAS event with meas %d we don\'t know about: %s',
            meas_id, event)
        return
    meas = state.measurements[meas_id]
    if event.ffmeas_type == 'PARAMS_SENT':
        return notif_ffmeas_event_PARAMS_SENT(meas, event)
    elif event.ffmeas_type == 'PARAMS_OK':
        return notif_ffmeas_event_PARAMS_OK(meas, event, state)
    elif event.ffmeas_type == 'BW_REPORT':
        return notif_ffmeas_event_BW_REPORT(meas, event)
    log.warn(
        'Ignoring FF_MEAS event because unrecognized/unhandled type '
        '%s: %s', event.ffmeas_type, event)
    return


def _notif_ffmeas_event(event: FFMeasEvent):
    ''' Called from stem to tell us about FF_MEAS events.

    We are currently in a different thread. We tell the main thread's loop
    (in a threadsafe manner) to handle this event.
    '''
    loop.call_soon_threadsafe(partial(notif_ffmeas_event, event))


def notif_measurer_connected(measr: MeasrProtocol, state: State):
    ''' Called from MeasrProtocol when a connection is successfully made from a
    measurer '''
    state.measurers.add(measr)
    log.debug('Now have %d measurers', len(state.measurers))


def notif_measurer_disconnected(measr: MeasrProtocol, state: State):
    ''' Called from MeasrProtocol when a connection with a measurer has been
    lost '''
    state.measurers.remove(measr)
    log.debug('Measurer lost. Now have %d', len(state.measurers))
    # TODO: need to do error stuff if they were a part of any measurements


def have_all_bw_reports(meas: Measurement) -> bool:
    ''' Check if we have the expected number of ``bg_reports`` and
    ``measr_reports`` for the given measurement

    :param meas_id: The measurement ID. If we don't know about it, we
        return ``False``
    :returns: ``True`` if we have the expected number of reports from all
        parties, else ``False`` i.e. because unrecognized ``meas_id`` or
        simply yet-incomplete measurement
    '''
    # For debug logging purposes, gather all the report list lengths here.
    # We could return early as soon as we find one that isn't long enough.
    # But this may help debug.
    #
    # The first item is the number of background reports. All other items
    # are the number of reports from each measurer
    counts = [len(meas.bg_reports)] + [
        len(measr_n_reports) for measr_n_reports
        in meas.measr_reports.values()]
    num_expect = meas.meas_duration
    log.debug(
        'Meas %d has %d/%d bg reports. Num measr reports: %s',
        meas.meas_id, counts[0], num_expect,
        ', '.join([str(_) for _ in counts[1:]]))
    # Count how many of the counts are less than the number needed. If
    # a non-zero number of the counts are too small, not done yet
    if len(['' for c in counts if c < num_expect]):
        log.debug('Meas %d not finished', meas.meas_id)
        return False
    log.info('Meas %d is finished', meas.meas_id)
    return True


def _notif_measurer_msg_ConnectedToRelay(
        measr: MeasrProtocol, message: msg.FFMsg, state: State):
    meas_id = message.orig.meas_id
    if meas_id not in state.measurements:
        log.info(
            'Received ConnectedToRelay for unknown meas %d, dropping.',
            meas_id)
        return
    meas = state.measurements[meas_id]
    meas.ready_measurers.add(measr)
    ready_measr = meas.ready_measurers
    all_measr = meas.measurers
    log.debug(
        '%d/%d measurers are ready for meas %d',
        len(ready_measr), len(all_measr), meas_id)
    if len(ready_measr) == len(all_measr):
        log.debug('Sending Go message for meas %d', meas_id)
        ret = tor_client.send_msg(
            state.tor_client, CoordStartMeas(
                meas.meas_id, meas.relay_fp, meas.meas_duration))
        if not ret.is_ok():
            fail_msg = msg.Failure(
                msg.FailCode.C_START_ACTIVE_MEAS, meas_id,
                extra_info=str(ret))
            log.error(fail_msg)
            for measr in ready_measr:
                assert measr.transport is not None
                measr.transport.write(fail_msg.serialize())
            del state.measurements[meas_id]
            return
        for measr in ready_measr:
            go_msg = msg.Go(meas_id)
            assert measr.transport is not None
            measr.transport.write(go_msg.serialize())
    return


def _notif_measurer_msg_BwReport(
        measr: MeasrProtocol, message: msg.FFMsg, state: State):
    meas_id = message.meas_id
    if meas_id not in state.measurements:
        log.info(
            'Received BwReport for unknown meas %d, dropping.', meas_id)
        return
    meas = state.measurements[meas_id]
    meas.measr_reports[measr].append((
        message.ts, message.sent, message.recv))
    if have_all_bw_reports(meas):
        return finish_measurement(meas, None)
    return


def notif_measurer_msg(
        measr: MeasrProtocol, message: msg.FFMsg,
        state: Optional[State] = None):
    ''' Receive a FFMsg object from one of our measurers '''
    global g_state
    if not state:
        state = g_state
    msg_type = type(message)
    if msg_type == msg.ConnectedToRelay:
        assert isinstance(message, msg.ConnectedToRelay)  # so mypy knows
        return _notif_measurer_msg_ConnectedToRelay(measr, message, state)
    elif msg_type == msg.BwReport:
        assert isinstance(message, msg.BwReport)  # so mypy knows
        return _notif_measurer_msg_BwReport(measr, message, state)
    log.warn('Dropping unexpected %s message from measurer', msg_type)


async def ensure_measurer_listen_socks(conf: ConfigParser) -> \
        Tuple[bool, Union[Server, str]]:
    ''' Create the listen sockets to which measurers will connect. On
    success, return True and the :class:`Server` listening for measurers.
    Otherwise return False and an operator-meaningful error message. '''
    # Get (host, port) from "host:port"
    addr_port = conf.getaddr('coord', 'listen_addr')
    if addr_port is None:
        return False, 'Don\'t know what to listen on'
    # Make sure TLS key material exists
    our_key = conf.getpath('coord', 'key')
    keydir = conf.getpath('coord', 'keydir')
    if not os.path.isfile(our_key):
        return False, '%s does not exist' % our_key
    if not os.path.isdir(keydir):
        return False, '%s does not exist' % keydir
    # Start building ssl context. This first bit is a helper that takes the
    # measurer certificate files and combines them into one big file
    # listing them all, since that's what python's ssl wants
    _, measr_cert_fname = _gen_concated_measr_cert_file(keydir, our_key)
    ssl_context = ssl.SSLContext()
    # Load our TLS private key and certificate
    ssl_context.load_cert_chain(our_key)
    # Load the certificate of the measurers
    ssl_context.load_verify_locations(measr_cert_fname)
    ssl_context.verify_mode = ssl.CERT_REQUIRED
    # Create the async task of opening this listen socks.
    try:
        server = await loop.create_server(
            MeasrProtocol,
            addr_port[0], addr_port[1],
            ssl=ssl_context,
            reuse_address=True)
    except Exception as e:
        return False, 'Unable to open listen sock(s): %s' % e
    else:
        for s in server.sockets:
            log.info('Listening on %s for measurers', s.getsockname())
        return True, server


async def ensure_ctrl_listen_socks(conf: ConfigParser) -> \
        Tuple[bool, Union[Server, str]]:
    ''' Create the listen sockets to which FF controllers will connect. On
    success, return True and the :class:`Server` listening for FF controllers.
    Otherwise return False and an operator-meaningful error message. '''
    addr_port = conf.getaddr('coord', 'ctrl_addr')
    try:
        server = await loop.create_server(
            CtrlProtocol,
            addr_port[0], addr_port[1],
            reuse_address=True)
    except Exception as e:
        return False, 'Unable to open listen sock(s): %s' % e
    else:
        for s in server.sockets:
            log.info('Listening on %s for FF controllers', s.getsockname())
        return True, server


async def ensure_listen_socks(conf: ConfigParser) -> \
        Tuple[bool, Union[Tuple[Server, Server]], str]:
    ''' Create all the listening sockets we need. On success return True and a
    tuple of all the :class:`Server` s. Otherwise return False and an
    operator-meaningful error message. '''
    success, measr_server_or_err = await asyncio.create_task(
        ensure_measurer_listen_socks(conf))
    if not success:
        return False, measr_server_or_err
    success, ctrl_server_or_err = await asyncio.create_task(
        ensure_ctrl_listen_socks(conf))
    if not success:
        return False, ctrl_server_or_err
    return True, (measr_server_or_err, ctrl_server_or_err)


def ensure_conn_w_tor(conf: ConfigParser) -> \
        Tuple[bool, Union[Controller, str]]:
    ''' Create a Tor client, connect to its control socket, authenticate, and
    return the :class:`Controller`. On success, return True and the controller.
    Otherwise return False and a operator-meaningful error message. '''
    # TODO: what happens if tor client disappears? Exception thrown? What??
    # And what should we do about it? Try to relaunch? Just die? Choose
    # **something** pastly/flashflow#29
    c = tor_client.launch(
        conf.getpath('tor', 'tor_bin'),
        conf.getpath('coord', 'tor_datadir'),
        conf.get('tor', 'torrc_extra_lines')
    )
    if not c:
        return False, 'Unable to launch and connect to tor client'
    c.add_event_listener(_notif_circ_event, EventType.CIRC)
    c.add_event_listener(_notif_ffmeas_event, EventType.FF_MEAS)
    return True, c


def write_measurement_results(meas: Measurement):
    ''' We have completed a measurement (maybe successfully) and should write
    out measurement results to our file. '''
    if not meas.should_save_data():
        return
    start_ts, end_ts = meas.start_and_end()
    results_logger.write_begin(meas.relay_fp, meas.meas_id, int(start_ts))
    # Take the minimum of send/recv from the relay's bg reports for each
    # second. These are untrusted results because the relay may have lied
    # about having a massive amount of background traffic
    bg_report_untrust = [(ts, min(s, r)) for ts, s, r in meas.bg_reports]
    # Always take the recv side of measurer reports since that's the only
    # side that definitely made it back from the relay
    measr_reports = []
    for measr_report in meas.measr_reports.values():
        lst = [(ts, r) for ts, _, r in measr_report]
        for ts, r in lst:
            results_logger.write_meas(
                meas.meas_id, int(ts), r)
        measr_reports.append([r for _, r in lst])
    # For each second, cap the amount of claimed bg traffic to the maximum
    # amount we will trust. I.e. if the relay is supposed to reserve no
    # more than 25% of its capacity for bg traffic, make sure the reported
    # background traffic is no more than 25% of all data we have for that
    # second.
    # TODO: make the fraction configurable
    bg_report_trust = []
    for sec_i, (ts, bg_untrust) in enumerate(bg_report_untrust):
        # The relay is supposed to be throttling its bg traffic such that
        # it is no greater than some fraction of total traffic.
        #     frac = bg / (bg + meas)
        # We know and trust meas. We know frac. Thus we can solve for the
        # maximum allowed bg:
        #     frac * bg + frac * meas = bg
        #     frac * bg - bg          = -frac * meas
        #     bg * (frac - 1)         = -frac * meas
        #     bg                      = (-frac * meas) / (frac - 1)
        #     bg                      = (frac * meas) / (1 - frac)
        frac = meas.bg_percent
        measured = sum([
            measr_report[sec_i] for measr_report in measr_reports])
        max_bg = int(frac * measured / (1 - frac))
        if bg_untrust > max_bg:
            log.warn(
                'Meas %d capping %s\'s reported bg to %d as %d is too '
                'much', meas.meas_id, meas.relay_fp, max_bg, bg_untrust)
        bg_report_trust.append(min(bg_untrust, max_bg))
        results_logger.write_bg(
            meas.meas_id, int(ts), bg_untrust, max_bg)
    # Calculate each second's aggregate bytes
    aggs = [
        sum(sec_i_vals) for sec_i_vals
        in zip(bg_report_trust, *measr_reports)]
    # Calculate the median over all seconds
    res = int(median(aggs))
    # Log as Mbit/s
    log.info(
        'Meas %d %s was measured at %.2f Mbit/s',
        meas.meas_id, meas.relay_fp, res*8/1e6)
    results_logger.write_end(meas.meas_id, int(end_ts))


def finish_measurement(
        meas: Measurement, fail_msg: Optional[msg.Failure], state: State):
    ''' We have finished a measurement, successful or not. Write out the
    results we have, tell everyone we are done, and forget about it. '''
    write_measurement_results(meas)
    if fail_msg is not None:
        for measr in meas.measurers:
            if measr.transport:
                measr.transport.write(fail_msg.serialize())
    del state.measurements[meas.meas_id]
    log.info(
        'Meas %d finished.%s Now %d measurements.',
        meas.meas_id,
        '' if not fail_msg else ' Err="' + str(fail_msg) + '".',
        len(state.measurements))


def begin_of_slot_cb():
    global g_state
    now = time.time()
    period_dur = g_state.conf.getint('coord', 'meas_period')
    meas_dur = g_state.conf.getint('meas_params', 'meas_duration')
    current_period = meas_period.current_period(now, period_dur)
    current_slot = meas_period.current_slot(now, period_dur, meas_dur)
    log.info(
        'It\'s the beginning of period %d slot %d',
        current_period, current_slot)
    # Cancel any still running measurements. It's too late. Time to go and
    # free up resources.
    if len(g_state.measurements):
        # Grab a copy of the measurements. The dictionary will be updated
        # in the :meth:`StateMachine._finish_measurement` call, which
        # breaks iteration
        measurements = [_ for _ in g_state.measurements.values()]
        for meas in measurements:
            log.warn(
                'Cleaning up after must-have-failed measurement %d',
                meas.meas_id)
            fail_msg = msg.Failure(
                msg.FailCode.C_END_OF_SLOT,
                meas.meas_id,
                None)
            finish_measurement(meas, fail_msg, g_state)
    assert not len(g_state.measurements)
    # Check if we've entered a new period. If so, need to generate new
    # schedule
    if current_period != g_state.state_file.get('last_meas_period'):
        begin_of_new_period(g_state)
    # If no measurements this slot, return early
    if current_slot not in g_state.schedule.slots:
        log.info('Nothing to do')
        schedule_next_begin_of_slot(g_state)
        return
    # Stuff to do, so send ourself the command to do said stuff
    measurements = g_state.schedule.slots[current_slot]
    for relay_fp, measr_meas_infos in measurements:
        log.info(
            'Would start meas of %s with %d measurers',
            relay_fp, len(measr_meas_infos))
        success, err_msg = notif_ctrl_message('measure ' + relay_fp)
        if not success:
            log.warn('Unable to start measurement: %s', err_msg)
    schedule_next_begin_of_slot(g_state)


def make_new_schedule(conf: ConfigParser, tor_client: Controller) -> \
        meas_period.Schedule:
    ''' Make a new measurement schedule, scheduling each relay we know
    about to be measured at some point during the existing measurement
    period (if now is not the very beginning of a period, we might schedule
    some for the past). '''
    relay_infos = get_relays_to_measure(
        tor_client, conf.getpath('coord', 'relay_filter_list'))
    measr_infos = get_measr_infos(conf)
    return meas_period.Schedule.gen(
        relay_infos, measr_infos,
        slots_in_period(conf),
        conf.getint('meas_params', 'num_circs'),
    )


def begin_of_new_period(state: State):
    # Need to throw away old schedule and make a new one.
    state.schedule = make_new_schedule(state.conf, state.tor_client)
    state.state_file.set(
        'meas_schedule', state.schedule.to_dict(), skip_write=True)
    # Update measurement period number
    meas_period_dur = state.conf.getint('coord', 'meas_period')
    curr_meas_period = meas_period.current_period(
        time.time(), meas_period_dur)
    state.state_file.set('last_meas_period', curr_meas_period)


def schedule_next_begin_of_slot(state: State):
    ''' Schedule the next call of the begin-of-slot callback function '''
    # Cancel any existing one. Safe to do even if already canceled or
    # executed
    if state.begin_of_slot_handle:
        state.begin_of_slot_handle.cancel()
    # Now for the new one
    now = time.time()
    meas_dur = state.conf.getint('meas_params', 'meas_duration')
    state.begin_of_slot_handle = loop.call_later(
        meas_period.time_till_next_slot(now+0.001, meas_dur),
        begin_of_slot_cb)


def connect_to_relay(meas: Measurement, state: State):
    ''' Start the given measurement off by connecting ourselves to the
    necessary relay '''
    # Sanity: we haven't launched a circuit for this measurement yet
    if meas.relay_circ is not None:
        log.error(
            'Ready to connect to relay, but meas %d already has circ %d',
            meas.meas_id, meas.relay_circ)
        return
    # Tell our tor to launch the circuit
    ret = tor_client.send_msg(
        state.tor_client,
        CoordStartMeas(meas.meas_id, meas.relay_fp, meas.meas_duration))
    # Make sure it is LAUNCHED. Launched just means circuit construction is
    # started. BUILT is when the circuit is successfully built, and comes
    # later.
    if not ret.is_ok():
        # TODO: record the failure somehow pastly/flashflow#18
        log.error(
            'Failed to launch circuit to %s: %s',
            meas.relay_fp, ret)
        return
    # We expect to see "250 LAUNCHED <circ_id>", e.g. "250 LAUNCHED 24".
    # Get the circuit id out and save it for later use.
    code, _, content = ret.content()[0]
    assert code == '250'
    parts = content.split()
    if len(parts) != 2 or parts[0] != 'LAUNCHED':
        # TODO: record the failure somehow pastly/flashflow#18
        log.error(
            'Did not expect body of message to be: %s', content)
        return
    meas.relay_circ = int(parts[1])
    state.measurements[meas.meas_id] = meas
    log.info(
        'Meas %d launched circ %d with relay %s',
        meas.meas_id, meas.relay_circ, meas.relay_fp)
    # That's all for now. We stay in this state until Tor tells us it has
    # finished building the circuit


def notif_ctrl_message(
        msg: str, state: Optional[State] = None) -> Tuple[bool, str]:
    ''' Called from CtrlProtocol when a controller has given us a command.
    Returns (True, '') if the message seems like a good, actionable
    message.  Otherwise returns False and a human-meaningful string with
    more information.  '''
    global g_state
    if not state:
        state = g_state
    words = msg.lower().split()
    if not len(words):
        return False, 'Empty command?'
    command = words[0]
    if command == 'measure':
        log.debug('told to measure %s', words[1])
        meas_id = next_meas_id()
        relay = find_relay(state.tor_client, words[1])
        if not relay:
            return False, 'No such relay ' + words[1]
        relay_fp = relay.fingerprint
        meas = Measurement(
            meas_id,
            relay_fp,
            {_ for _ in state.measurers},
            # Get these from consensus parameters? pastly/flashflow#30
            # pastly/flashflow#31
            state.conf.getint('meas_params', 'meas_duration'),
            state.conf.getfloat('meas_params', 'bg_percent'))
        loop.call_soon(partial(connect_to_relay, meas, state))
        return True, ''
    elif command == 'v3bw':
        generate_v3bw(state.conf)
        return True, ''
    elif command == 'ping':
        return False, 'pong'
    return False, 'Unknown ctrl command: ' + msg


def notif_sslerror(self, exc: ssl.SSLError, trans):
    ''' Called from the last-chance exception handler to tell us about TLS
    errors. For example, measurer connected to us with a bad client cert
    '''
    log.debug(
        'Someone (%s) failed to TLS handshake with us: %s',
        trans.get_extra_info('peername'), exc)
    trans.close()


def generate_v3bw(conf: ConfigParser):
    ''' Generate a v3bw file using our latest results for each relay. '''
    v3bw_fname = v3bw.gen(
        conf.getpath('v3bw', 'v3bw'),
        conf.getpath('coord', 'results_log'),
        conf.getfloat('v3bw', 'max_results_age'))
    log.debug(v3bw_fname)


def cleanup(state: State):
    ''' Cleanup all the State object while being very careful to not allow any
    exceptions to bubble up. Use this when in an error state and you want
    to cleanup before starting over or just dying. '''
    if hasattr(state, 'meas_server') and state.meas_server:
        log.info('cleanup: closing listening sockets for measurers')
        try:
            state.meas_server.close()
        except Exception as e:
            log.error('Error closing listening sockets: %s', e)
    if hasattr(state, 'ctrl_server') and state.ctrl_server:
        log.info('cleanup: closing listening sockets for controllers')
        try:
            state.ctrl_server.close()
        except Exception as e:
            log.error('Error closing listening sockets: %s', e)
    if hasattr(state, 'tor_client') and state.tor_client:
        log.info('cleanup: closing tor')
        try:
            state.tor_client.close()
        except Exception as e:
            log.error('Error closing tor: %s', e)


def die():
    ''' End execution of the program. '''
    loop.stop()


def gen_parser(sub) -> ArgumentParser:
    ''' Add the cmd line options for this FlashFlow command '''
    d = 'Run as a FlashFlow coordinator.'
    p = sub.add_parser('coord', description=d)
    return p


# This function needs **some sort** of type annotation so that mypy will check
# the things it does. Adding the return value (e.g. '-> None') is enough
def main(args, conf) -> None:
    global g_state
    g_state = State(conf)
    os.makedirs(conf.getpath('coord', 'datadir'), mode=0o700, exist_ok=True)
    os.makedirs(conf.getpath('coord', 'keydir'), mode=0o700, exist_ok=True)
    _ensure_relays_txt(conf)
    loop.set_exception_handler(_exception_handler)
    # Open listening sockets
    success, servers_or_err = loop.run_until_complete(
        ensure_listen_socks(conf))
    if not success:
        log.error(servers_or_err)
        return
    measr_server, ctrl_server = servers_or_err
    # Get a tor client
    success, tor_client_or_err = ensure_conn_w_tor(conf)
    if not success:
        log.error(tor_client_or_err)
        return
    tor_client = tor_client_or_err
    g_state.attach_sockets(tor_client, measr_server, ctrl_server)
    # Kick off the first call of our beginning-of-slot event
    schedule_next_begin_of_slot(g_state)
    log.info('Started FlashFlow %s coordinator', FF_VERSION)
    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()
    return
