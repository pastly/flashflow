from argparse import ArgumentParser
from functools import partial
from traceback import StackSummary
import asyncio
import enum
import logging
import ssl
import time
import os
from configparser import ConfigParser
from stem import CircStatus  # type: ignore
from stem.control import Controller, EventType  # type: ignore
from stem.response.events import CircuitEvent, FFMeasEvent  # type: ignore
from typing import Tuple, Union, Set, Dict
from .. import tor_client
from .. import msg
from .. import __version__ as FF_VERSION
from ..tor_ctrl_msg import MeasrStartMeas


class CoordProtocol(asyncio.Protocol):
    transport = None

    def connection_made(self, transport):
        log.debug('Connected to coord')
        self.transport = transport

    def connection_lost(self, exc):
        global state
        log.error('Lost connection with coord')
        cleanup(state)
        # TODO: don't die just because the coord went away!
        die()

    def data_received(self, data: bytes):
        ''' Receive data from the coordinator. Parse it into a FFMsg and tell
        other code about the message.

        It's possible that this is called before the entire message is
        received. In that case, we'll need to edit this function to buffer
        bytes until the entire message has arrived.  '''
        log.info('Received %d bytes: %s', len(data), data)
        m = msg.FFMsg.deserialize(data)
        notif_coord_msg(m)


class Measurement:
    ''' State related to a single measurement. '''
    #: keep a copy of :class:`flashflow.msg.ConnectToRelay` command so we can
    #: send it back to the coord when we're ready to go (or have failed)
    connect_msg: msg.ConnectToRelay
    #: Our circuit ids with the relay. Filled in once we know what they are
    #: (they're launched) but not yet bullt
    circs: Set[int]
    #: Our built circuit ids with the relay. Filled in as we learn of launched
    #: circuits becoming built.
    ready_circs: Set[int]
    #: Our circuit ids that we've been told have CLOSED or FAILED at any point
    bad_circs: Set[int]

    def __init__(self, connect_msg: msg.ConnectToRelay):
        self.connect_msg = connect_msg
        self.circs = set()
        self.ready_circs = set()
        self.bad_circs = set()

    @property
    def meas_id(self) -> int:
        ''' The measurement ID '''
        return self.connect_msg.meas_id

    @property
    def relay_fp(self) -> str:
        ''' The fingerprint of the relay to measure '''
        return self.connect_msg.fp

    @property
    def meas_duration(self) -> int:
        ''' The duration, in seconds, that active measurement should last. '''
        return self.connect_msg.dur

    @property
    def alloc_bw(self) -> int:
        ''' The amount of bandwidth, in bytes/second, the client should
        allocate for this measurement '''
        return self.connect_msg.bw

    @property
    def waiting_circs(self) -> Set[int]:
        ''' Circs that we have LAUNCHED but have not yet added to ready_circs
        because we haven't seen BUILT yet.

        Note that as far as this function is concerned, there's no such thing
        as a circuit becoming un-BUILT. This functiion doesn't know anything
        about circuits closing. Other code needs to manipulate circs and
        ready_circs as it deems fit.
        '''
        return self.circs - self.ready_circs


class State:
    ''' Global state for the measurer process '''
    #: Our Tor client, set in main
    tor_client: Controller
    #: how we communicate with the coord
    coord_trans: asyncio.WriteTransport
    #: how we communicate with the coord
    coord_proto: CoordProtocol
    #: state specific to each measurement, obviously
    measurements: Dict[int, Measurement]

    def __init__(self):
        self.measurements = {}


class CoordConnRes(enum.Enum):
    ''' Part of the return value of :meth:`try_connect_to_coord`.  '''
    #: We successfully connected to the coord, shook our TLS hands, and all is
    #: well.
    SUCCESS = enum.auto()
    #: We were not successful, but whatever happened may be temporary and it's
    #: logical to try connecting again in the future.
    RETRY_ERROR = enum.auto()
    #: We were not successful, and trying again in the future is extremely
    #: unlikely to be successful. We should give up.
    FATAL_ERROR = enum.auto()


async def try_connect_to_coord(
    addr_port: Tuple[str, int],
    our_key: str,
    coord_cert: str,
) -> Tuple[
    CoordConnRes, Union[
        str, Tuple[asyncio.BaseTransport, asyncio.BaseProtocol]]]:
    ''' Try to connect to the coordinator at the given (host, port) tuple.
    Perform the TLS handshake using our client TLS key in the file `our_key`
    and only trusting the coord server cert in the file `coord_cert`.

    Returns a tuple in all cases. The first item indicates success with
    CoordConnRes. If it is an *_ERROR, then the second item is a string with
    more details. If it is SUCCESS, then the second item is the transport and
    protocol with the coordinator.

    This function is a coroutine and all exceptions **should** be handled
    within this function's body. If they aren't, that's a programming error.
    '''
    if not os.path.isfile(our_key):
        return CoordConnRes.FATAL_ERROR, our_key + ' does not exist'
    if not os.path.isfile(coord_cert):
        return CoordConnRes.FATAL_ERROR, coord_cert + ' does not exist'
    ssl_context = ssl.SSLContext()
    # Load our TLS private key and certificate
    ssl_context.load_cert_chain(our_key)
    # Load the certificate of the coord
    ssl_context.load_verify_locations(coord_cert)
    ssl_context.verify_mode = ssl.CERT_REQUIRED
    try:
        res = await loop.create_connection(
            CoordProtocol,
            addr_port[0],
            addr_port[1],
            ssl=ssl_context,
        )
    except OSError as e:
        return CoordConnRes.RETRY_ERROR, str(e)
    return CoordConnRes.SUCCESS, res


def _exception_handler(loop, context):
    global state
    log.error('%s', context['message'])
    if 'exception' in context:
        log.error(context['exception'])
    if 'handle' in context:
        log.error(context['handle'])
    if 'source_traceback' in context:
        log.error('Traceback:')
        summary = StackSummary.from_list(context['source_traceback'])
        for line_super in summary.format():
            # The above line has multiple lines in it
            for line in line_super.split('\n'):
                if len(line):
                    log.error('  %s', line)
    else:
        log.error('Traceback not available. Run with PYTHONASYNCIODEBUG=1')
    cleanup(state)
    die()


# # Not sure if this would actually work here. Maybe add to the logging config
# # file?
# # https://docs.python.org/3.6/library/asyncio-dev.html#logging
# logging.getLogger('asyncio').setLevel(logging.WARNING)
log = logging.getLogger(__name__)
loop = asyncio.get_event_loop()
state = State()


def _notif_circ_event_BUILT(
        state: State, meas: Measurement, event: CircuitEvent):
    circ_id = int(event.id)
    # Should have already been checked
    if circ_id not in meas.circs:
        return
    meas.ready_circs.add(circ_id)
    log.debug(
        'Circ %d added to meas %d\'s built circs. Now have %d/%d',
        circ_id, meas.meas_id, len(meas.ready_circs), len(meas.circs))
    # If all are built, then tell coord this measurement is ready
    if len(meas.ready_circs) < len(meas.circs):
        return
    log.info('Meas %d built all circs', meas.meas_id)
    state.coord_trans.write(msg.ConnectedToRelay(meas.connect_msg).serialize())


def _notif_circ_event_CLOSED_FAILED(
        state: State, meas: Measurement, event: CircuitEvent):
    circ_id = int(event.id)
    meas.bad_circs.add(circ_id)
    log.info(
        'Meas %d\'s circ %d is now closed/failed: %s',
        meas.meas_id, circ_id, event)


def _notif_circ_event(event: CircuitEvent):
    ''' The real CIRC event handler, in the main thread's loop.

    Receive a CIRC event from our tor client.
    '''
    global state
    circ_id = int(event.id)
    # Make sure it's a circuit we care about
    all_circs: Set[int] = set.union(
        # in case there's no measurements, add empty set to avoid errors
        set(),
        *[meas.circs for meas in state.measurements.values()])
    waiting_circs: Set[int] = set.union(
        # in case there's no measurements, add empty set to avoid errors
        set(),
        *[meas.waiting_circs for meas in state.measurements.values()])
    if circ_id not in all_circs:
        # log.warn(
        #     'Ignoring CIRC event not for us. %d not in any '
        #     'measurement\'s set of all circuits',
        #     circ_id)
        return
    if event.status == CircStatus.BUILT:
        if circ_id not in waiting_circs:
            log.warn(
                'CIRC BUILT event for circ %d we do care about but that '
                'isn\'t waiting. Shouldn\'t be possible. %s. Ignoring.',
                circ_id, event)
            return
        # Tell all interested Measurements (should just be one, but do all
        # that claim to care about this circuit, just in case) that the
        # circuit is built
        for meas in state.measurements.values():
            if circ_id not in meas.circs:
                continue
            _notif_circ_event_BUILT(state, meas, event)
        return
    elif event.status in {CircStatus.LAUNCHED, CircStatus.EXTENDED}:
        # ignore these
        return
    elif event.status in {CircStatus.CLOSED, CircStatus.FAILED}:
        # Tell all interested Measurements (should just be one, but do all
        # that claim to care about this circuit, just in case) that the
        # circuit has closed or failed
        for meas in state.measurements.values():
            if circ_id not in meas.circs:
                continue
            _notif_circ_event_CLOSED_FAILED(state, meas, event)
        return
    # It's for us, but don't know how to handle it yet
    log.warn('Not handling CIRC event for us: %s', event)


def notif_circ_event(event: CircuitEvent):
    ''' Called from stem to tell us about circuit events.

    We are currently in a different thread. We tell the main thread's loop
    (in a threadsafe manner) to handle this event.
    '''
    loop.call_soon_threadsafe(partial(_notif_circ_event, event))


def _notif_ffmeas_event_END(state: State, event: FFMeasEvent):
    log.info(
        'Tor client tells us meas %d finished %ssuccessfully%s',
        event.meas_id, '' if event.success else 'un',
        '. Cleaning up.' if event.meas_id in state.measurements else
        ', but we don\'t know about it. Dropping.')
    if event.meas_id not in state.measurements:
        return
    del state.measurements[event.meas_id]
    return


def _notif_ffmeas_event_BW_REPORT(state: State, event: FFMeasEvent):
    log.debug(
        'Forwarding report of %d/%d sent/recv meas bytes',
        event.sent, event.recv)
    report = msg.BwReport(event.meas_id, time.time(), event.sent, event.recv)
    state.coord_trans.write(report.serialize())


def _notif_ffmeas_event(event: FFMeasEvent):
    ''' The real FF_MEAS event handler, in the main thread's loop.

    We look for:
    - per-second BW_REPORTs of the amount of measurement traffic sent and
    received, and we will fowarded those on to the coordinator.
    - a END message at the end signally success.
    '''
    global state
    if event.ffmeas_type == 'BW_REPORT':
        return _notif_ffmeas_event_BW_REPORT(state, event)
    elif event.ffmeas_type == 'END':
        return _notif_ffmeas_event_END(state, event)
    log.warn(
        'Unexpected FF_MEAS event type %s. Dropping.', event.ffmeas_type)
    return


def notif_ffmeas_event(event: FFMeasEvent):
    ''' Called from stem to tell us about FF_MEAS events.

    We are currently in a different thread. We tell the main thread's loop
    (in a threadsafe manner) to handle this event.
    '''
    loop.call_soon_threadsafe(partial(_notif_ffmeas_event, event))


def _notif_coord_msg_Go(state, go_msg: msg.Go):
    meas_id = go_msg.meas_id
    if meas_id not in state.measurements:
        fail_msg = msg.Failure(msg.FailCode.M_UNKNOWN_MEAS_ID, meas_id)
        log.error(fail_msg)
        state.coord_trans.write(fail_msg.serialize())
        # TODO: cleanup Measurement
        return
    meas = state.measurements[meas_id]
    start_msg = MeasrStartMeas(
        meas.meas_id, meas.relay_fp, len(meas.ready_circs),
        meas.alloc_bw, meas.meas_duration)
    ret = tor_client.send_msg(state.tor_client, start_msg)
    if not ret.is_ok():
        fail_msg = msg.Failure(msg.FailCode.M_START_ACTIVE_MEAS, meas_id)
        log.error(fail_msg)
        state.coord_trans.write(fail_msg.serialize())
        # TODO: cleanup Measurement
        return


def _notif_coord_msg_Failure(state: State, fail_msg: msg.Failure):
    meas_id = fail_msg.meas_id
    log.warn(
        'Received FailCode %s regarding meas id %s',
        fail_msg.code.name, meas_id)
    if meas_id is None:
        return
    if meas_id not in state.measurements:
        log.info(
            'Unknown measurement %d in Failure message, so nothing to do',
            meas_id)
        return
    # TODO: uhh ... close circuits or something? Other clean up?


def _notif_coord_msg_ConnectToRelay(state: State, message: msg.ConnectToRelay):
    meas_id = message.meas_id
    if meas_id in state.measurements:
        fail_msg = msg.Failure(msg.FailCode.M_DUPE_MEAS_ID, meas_id)
        log.error(fail_msg)
        state.coord_trans.write(fail_msg.serialize())
        return
    meas = Measurement(message)
    ret = tor_client.send_msg(
        state.tor_client,
        MeasrStartMeas(
            meas.meas_id, meas.relay_fp, message.n_circs,
            message.bw, meas.meas_duration))
    # Make sure the circuit launches went well. Note they aren't built yet.
    # It's just that tor found nothing obviously wrong with trying to build
    # these circuits.
    if not ret.is_ok():
        fail_msg = msg.Failure(
            msg.FailCode.LAUNCH_CIRCS, meas_id,
            extra_info=str(ret))
        log.error(fail_msg)
        state.coord_trans.write(fail_msg.serialize())
        return
    # We expect to see "250 FF_MEAS 0 LAUNCHED CIRCS=1,2,3,4,5", where the
    # 0 is the measurement ID we told the tor client, and the actual list
    # of launched circuits is CIRCS the comma-separated list
    code, _, content = ret.content()[0]
    # Already checked this above with ret.is_ok()
    assert code == '250'
    parts = content.split()
    if len(parts) != 4 or \
            not parts[0] == 'FF_MEAS' or \
            not parts[2] == 'LAUNCHED' or \
            not parts[3].startswith('CIRCS='):
        fail_msg = msg.Failure(
            msg.FailCode.MALFORMED_TOR_RESP, meas_id,
            extra_info=str(ret))
        log.error(fail_msg)
        state.coord_trans.write(fail_msg.serialize())
        return
    meas.circs.update({
        int(circ_id_str) for circ_id_str in
        parts[3].split('=')[1].split(',')
    })
    log.info(
        'Launched %d circuits with relay %s: %s', len(meas.circs),
        meas.relay_fp, meas.circs)
    state.measurements[meas_id] = meas
    # That's all for now. We stay in this state until Tor tells us it has
    # finished building all circuits


def notif_coord_msg(message: msg.FFMsg):
    global state
    msg_type = type(message)
    # The asserts below are for shutting up mypy
    if msg_type == msg.ConnectToRelay:
        assert isinstance(message, msg.ConnectToRelay)
        return _notif_coord_msg_ConnectToRelay(state, message)
    elif msg_type == msg.Failure:
        assert isinstance(message, msg.Failure)
        return _notif_coord_msg_Failure(state, message)
    elif msg_type == msg.Go:
        assert isinstance(message, msg.Go)
        return _notif_coord_msg_Go(state, message)
    log.warn(
        'Unexpected/unhandled %s message. Dropping. %s',
        msg_type, message)


def ensure_conn_w_tor(conf: ConfigParser):
    ''' Create a Tor client, connect to its control socket, authenticate, and
    return the :class:`Controller`. On success, return True and the controller.
    Otherwise return False and an operator-meaningful error message. '''
    # TODO: what happens if tor client disappears? Exception thrown? What??
    # And what should we do about it? Try to relaunch? Just die? Choose
    # **something**
    c = tor_client.launch(
        conf.getpath('tor', 'tor_bin'),
        conf.getpath('measurer', 'tor_datadir'),
        conf.get('tor', 'torrc_extra_lines')
    )
    if not c:
        return False, 'Unable to launch and connect to tor client'
    c.add_event_listener(notif_circ_event, EventType.CIRC)
    c.add_event_listener(notif_ffmeas_event, EventType.FF_MEAS)
    return True, c


async def ensure_conn_w_coord(conf: ConfigParser, delay: float) -> \
        Tuple[bool, Union[
            Tuple[asyncio.BaseTransport, asyncio.BaseProtocol], str]]:
    ''' Repeatedly try connecting to the coordinator until we are successful or
    have a fatal error warranting completely giving up on life.

    This function uses asynchronous python: the connection is represented
    by a transport and protocol, and we try connecting asynchronously and
    use a callback to find out the result. That said, the work done here
    should probably be the only thing going on.
    '''
    # TODO: what if connection goes away?
    # Get the (host, port) from "host:port"
    coord_addr_port = conf.getaddr('measurer', 'coord_addr')
    if coord_addr_port is None:
        return False, 'Don\'t know where coord is'
    # Repeatedly try to connect to the coordinator. Will exit this infinite
    # loop upon success or fatal error
    while True:
        try:
            success_code, stuff_or_error = await try_connect_to_coord(
                coord_addr_port,
                conf.getpath('measurer', 'key'),
                conf.getpath('measurer', 'coord_cert'))
        except Exception as e:
            return False, str(e)
        else:
            if success_code == CoordConnRes.FATAL_ERROR:
                return False, 'Fatal error connecting to '\
                    'coordinator: %s' % (stuff_or_error,)
            elif success_code == CoordConnRes.RETRY_ERROR:
                delay = min(2 * delay, 60)
                log.warn(
                    'Unable to connect to coordinator: %s. Retrying in %.2fs.',
                    stuff_or_error, delay)
                await asyncio.sleep(delay)
            assert success_code == CoordConnRes.SUCCESS
            assert not isinstance(stuff_or_error, str)
            coord_trans, coord_proto = stuff_or_error
            return True, (coord_trans, coord_proto)


def cleanup(state: State):
    ''' Cleanup all of the State object while being very careful to not allow
    any exceptions to bubble up. Use this when in an error state and you want
    to cleanup before starting over or just dying. '''
    if hasattr(state, 'tor_client') and state.tor_client:
        log.info('cleanup: closing tor')
        try:
            state.tor_client.close()
        except Exception as e:
            log.error('Error closing tor: %s', e)
    if hasattr(state, 'coord_trans') and state.coord_trans:
        log.info('cleanup: closing coord transport')
        try:
            state.coord_trans.close()
        except Exception as e:
            log.error('Error closing transport with coord: %s', e)
    if hasattr(state, 'coord_proto') and state.coord_proto:
        # nothing to do
        pass
    if hasattr(state, 'measurements') and state.measurements:
        log.info(
            'cleanup: forgetting about %d measurements',
            len(state.measurements))
        state.measurements = {}


def die():
    ''' End execution of the program. '''
    loop.stop()


def gen_parser(sub) -> ArgumentParser:
    ''' Add the cmd line options for this FlashFlow command '''
    d = 'Run as a FlashFlow measurer.'
    p = sub.add_parser('measurer', description=d)
    return p


# This function needs **some sort** of type annotation so that mypy will check
# the things it does. Adding the return value (e.g. '-> None') is enough
def main(args, conf) -> None:
    global state
    os.makedirs(conf.getpath('measurer', 'datadir'), mode=0o700, exist_ok=True)
    os.makedirs(conf.getpath('measurer', 'keydir'), mode=0o700, exist_ok=True)
    # Get a tor client
    success, tor_client_or_err = ensure_conn_w_tor(conf)
    if not success:
        log.error(tor_client_or_err)
        return
    state.tor_client = tor_client_or_err
    # Connect to coordinatorr
    success, stuff_or_err = loop.run_until_complete(
        ensure_conn_w_coord(conf, 1))
    if not success:
        log.error(stuff_or_err)
        return
    state.coord_trans, state.coord_proto = stuff_or_err
    # machine = StateMachine(conf)
    loop.set_exception_handler(_exception_handler)
    # loop.call_soon(machine.change_state_starting)
    log.info('Started FlashFlow %s measurer', FF_VERSION)
    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()
    return
